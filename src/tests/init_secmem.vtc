# looks like -*- vcl -*-

varnishtest "init secmem in the init function"

# Default initialization of secure memory
varnish v1 -vcl {
	import gcrypt from "${vmod_topbuild}/src/.libs/libvmod_gcrypt.so";
	backend b { .host = "${bad_ip}"; }

	sub vcl_init {
		gcrypt.init(INIT_SECMEM);
		gcrypt.init(FINISH);
	}
} -start

# Start new Varnish instances for further tests.
varnish v1 -stop

# Initializing secure memory twice causes libgcrypt to log an error,
# but the VMOD can proceed.
varnish v2 -vcl {
	import gcrypt from "${vmod_topbuild}/src/.libs/libvmod_gcrypt.so";
	backend b { .host = "${bad_ip}"; }

	sub vcl_init {
		gcrypt.init(INIT_SECMEM);
		gcrypt.init(INIT_SECMEM, 32KB);
		gcrypt.init(FINISH);
	}
} -start

logexpect l2 -v v2 -d 1 -g raw -q "Error" {
	expect * 0 Error "^libgcrypt log message follows .ERROR.:"
	expect * = Error ".+"
} -run

# Initializing secure memory to 0 bytes is the same as disabling it.
varnish v2 -stop
varnish v3 -vcl {
	import gcrypt from "${vmod_topbuild}/src/.libs/libvmod_gcrypt.so";
	backend b { .host = "${bad_ip}"; }

	sub vcl_init {
		gcrypt.init(INIT_SECMEM, 0B);
		gcrypt.init(FINISH);
	}
} -start

varnish v3 -errvcl {vmod gcrypt error: secure memory not enabled in aes constructor} {
	import gcrypt from "${vmod_topbuild}/src/.libs/libvmod_gcrypt.so";
	import blobcode;
	backend b { .host = "${bad_ip}"; }

	sub vcl_init {
		new k = blobcode.blob(HEX, "00000000000000000000000000000000");
		new aes = gcrypt.symmetric(AES, ECB, key=k.get(), secure=true);
	}
}

# Secure memory is enabled by default
varnish v3 -stop
varnish v4 -vcl {
	import gcrypt from "${vmod_topbuild}/src/.libs/libvmod_gcrypt.so";
	import blobcode;
	backend b { .host = "${bad_ip}"; }

	sub vcl_init {
		gcrypt.init(FINISH);
		new k = blobcode.blob(HEX, "00000000000000000000000000000000");
		new aes = gcrypt.symmetric(AES, ECB, key=k.get(), secure=true);
	}
} -start
