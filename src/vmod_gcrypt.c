/*-
 * Copyright 2017 UPLEX - Nils Goroll Systemoptimierung
 * All rights reserved.
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/* for strdup() */
#define _POSIX_C_SOURCE 200809L

#include "config.h"

#include <gcrypt.h>
#include <string.h>
#include <pthread.h>
#include <errno.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <float.h>

#include "vcl.h"
#include "cache/cache.h"
#include "vrt.h"
#include "vas.h"
#include "vdef.h"
#include "vqueue.h"

#include "vcc_if.h"
#include "vmod_gcrypt.h"
#include "padding.h"

#define str(s) xstr(s)
#define xstr(s) #s

#define MIN_GCRYPT_VERSION "1.6.3"

#define ERR(ctx, msg) \
        errmsg((ctx), "vmod gcrypt error: " msg)

#define VERR(ctx, fmt, ...) \
        errmsg((ctx), "vmod gcrypt error: " fmt, __VA_ARGS__)

#define ERRNOMEM(ctx, msg) \
        ERR((ctx), msg ", out of space")

#define VERRNOMEM(ctx, fmt, ...) \
        VERR((ctx), fmt ", out of space", __VA_ARGS__)

struct vmod_gcrypt_symmetric {
	unsigned		magic;
#define VMOD_GCRYPT_SYMMETRIC_MAGIC 0x82c7ffe2
	pthread_key_t		hdk;
	char			*vcl_name;
	void			*key;
	size_t			keylen;
	enum padding		padding;
	int			algo;
	int			mode;
	unsigned int		flags;
};

struct filedata {
	unsigned		magic;
#define VMOD_GCRYPT_FILEDATA_MAGIC 0xb6250b0e
	VSLIST_ENTRY(filedata)	list;
	void			*contents;
	size_t			len;
};

struct rnd_bitmap {
	uint64_t	bits;
	uint8_t		nbits;
};

struct rnd_bool {
	unsigned		magic;
#define VMOD_GCRYPT_RND_BOOL_MAGIC 0x7d0a3e42
	struct rnd_bitmap	bitmap[2];
};

VSLIST_HEAD(filedata_head, filedata);

static const char *gcrypt_version = NULL;
static int secmem_enabled = 1;
static pthread_once_t rnd_bool_once = PTHREAD_ONCE_INIT;
static pthread_key_t rnd_boolk;
static int rnd_boolk_inited = 0;

static void
errmsg(VRT_CTX, const char *fmt, ...)
{
	va_list args;

	va_start(args, fmt);
	if (ctx->method == VCL_MET_INIT) {
		AN(ctx->msg);
		VSB_vprintf(ctx->msg, fmt, args);
		VRT_handling(ctx, VCL_RET_FAIL);
	}
	else if (ctx->vsl)
		VSLbv(ctx->vsl, SLT_VCL_Error, fmt, args);
	else
		/* Should this ever happen in vcl_fini() ... */
		VSL(SLT_VCL_Error, 0, fmt, args);
	va_end(args);
}

/* Event function */

static void __match_proto__(gcry_handler_log_t)
gcrypt_logger(void *priv, int level, const char *fmt, va_list args)
{
	enum VSL_tag_e tag = SLT_Debug;
	const char *lvl = "unknown log level";

	(void) priv;
	switch(level) {
	case GCRY_LOG_CONT:
		lvl = "continuation";
		break;
	case GCRY_LOG_INFO:
		lvl = "INFO";
		break;
	case GCRY_LOG_WARN:
		lvl = "WARN";
		break;
	case GCRY_LOG_ERROR:
		lvl = "ERROR";
		tag = SLT_Error;
		break;
	case GCRY_LOG_FATAL:
		lvl = "FATAL";
		tag = SLT_Error;
		break;
	case GCRY_LOG_BUG:
		lvl = "BUG";
		tag = SLT_Error;
		break;
	case GCRY_LOG_DEBUG:
		lvl = "DEBUG";
		break;
	default:
		VSL(SLT_Error, 0, "Unknown libgcrypt log level %d", level);
	}
	VSL(tag, 0, "libgcrypt log message follows (%s):", lvl);
	VSLv(tag, 0, fmt, args);
}

static void  __match_proto__(gcry_handler_error_t)
gcrypt_fatal(void *priv, int err, const char *text)
{
	(void) priv;
	if (text == NULL)
		text = gpg_strerror(err);
	VSL(SLT_Error, 0, "libgcrypt fatal error, code=%d (%s), panic follows",
	    err, text);
	AZ(text);
}

int __match_proto__(vmod_event_f)
event(VRT_CTX, struct vmod_priv *priv, enum vcl_event_e e)
{
	(void) ctx;
	(void) priv;

	if (e == VCL_EVENT_LOAD) {
		if (gcrypt_version != NULL)
			return 0;
		gcry_set_log_handler(gcrypt_logger, NULL);
		gcry_set_fatalerror_handler(gcrypt_fatal, NULL);
		gcrypt_version = gcry_check_version(MIN_GCRYPT_VERSION);
		if (gcrypt_version == NULL) {
			VSL(SLT_VCL_Error, 0,
			    "libgcrypt initialization failed");
			return 1;
		}
	}
	return 0;
}

/* Function init */

VCL_VOID
vmod_init(VRT_CTX, VCL_ENUM cmd, VCL_BYTES n)
{
	gcry_error_t err = GPG_ERR_NO_ERROR;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	AN(cmd);
	if (ctx->method != VCL_MET_INIT) {
		ERR(ctx, "gcrypt.init() is only legal in vcl_init");
		return;
	}
	if (gcry_control(GCRYCTL_INITIALIZATION_FINISHED_P)) {
		VSL(SLT_Debug, 0, "libgcrypt initialization already finished");
		return;
	}

	if (strcmp(cmd, "FINISH") == 0) {
		if ((err = gcry_control(GCRYCTL_INITIALIZATION_FINISHED))
		    != GPG_ERR_NO_ERROR)
			VERR(ctx, "Cannot finish initialization in "
			     "gcrypt.init(): %s/%s", gcry_strsource(err),
			     gcry_strerror(err));
		return;
	}
	if (strcmp(cmd, "INIT_SECMEM") == 0) {
		assert(n >= 0);
		if ((err = gcry_control(GCRYCTL_INIT_SECMEM, n))
		    != GPG_ERR_NO_ERROR)
			VERR(ctx, "Cannot initialize secure memory to %d bytes "
			     "in gcrypt.init(): %s/%s", n, gcry_strsource(err),
			     gcry_strerror(err));
		secmem_enabled = n;
		return;
	}
	if (strcmp(cmd, "DISABLE_SECMEM") == 0) {
		if ((err = gcry_control(GCRYCTL_DISABLE_SECMEM))
		    != GPG_ERR_NO_ERROR)
			VERR(ctx, "Cannot disable secure memory in "
			     "gcrypt.init(): %s/%s", gcry_strsource(err),
			     gcry_strerror(err));
		secmem_enabled = 0;
		return;
	}
	WRONG("Illegal cmd enum");
}

/* Object symmetric */

/* Destructor function for cipher handles pointed to by pthread keys */
static void
hdk_fini(void *p)
{
	gcry_cipher_hd_t *hd;

	AN(p);
	hd = (gcry_cipher_hd_t *)p;
	gcry_cipher_close(*hd);
	free(p);
}

VCL_VOID
vmod_symmetric__init(VRT_CTX, struct vmod_gcrypt_symmetric **symmetricp,
		     const char *vcl_name, VCL_ENUM ciphers, VCL_ENUM modes,
		     VCL_ENUM paddings, VCL_BLOB key, VCL_BOOL secure,
		     VCL_BOOL cbc_cts)
{
	struct vmod_gcrypt_symmetric *symmetric;
	gcry_cipher_hd_t hd;
	int algo = GCRY_CIPHER_NONE, mode = GCRY_CIPHER_MODE_NONE;
	unsigned int flags = 0;
	enum padding padding = _MAX_PADDING;
	gcry_error_t err = GPG_ERR_NO_ERROR;
	size_t len;
	pthread_key_t hdk;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	AN(symmetricp);
	AZ(*symmetricp);
	AN(vcl_name);
	AN(ciphers);
	AN(modes);
	AN(paddings);
	if (!gcry_control(GCRYCTL_INITIALIZATION_FINISHED_P)) {
		VERR(ctx,
		     "libgcrypt initialization not finished in %s constructor",
		     vcl_name);
		return;
	}
	if (key == NULL || key->priv == NULL) {
		VERR(ctx, "key is NULL in %s constructor", vcl_name);
		return;
	}
	assert(key->len >= 0);
	if (secure && !secmem_enabled) {
		VERR(ctx, "secure memory not enabled in %s constructor",
		     vcl_name);
		return;
	}

#define CIPHER(e, m) if (strcmp(ciphers, str(e)) == 0) algo = m;
#include "ciphers.h"
#undef CIPHER
	if (algo == GCRY_CIPHER_NONE)
		WRONG("Illegal cipher enum");
	if ((len = gcry_cipher_get_algo_keylen(algo)) == 0) {
		VERR(ctx, "Cannot determine key length for %s cipher in %s "
		     "constructor", ciphers, vcl_name);
		return;
	}
	if ((unsigned)key->len > len) {
		VERR(ctx, "Key length %d is longer than the maximum supported "
		     "length %zu for %s cipher in %s constructor", key->len,
		     len, ciphers, vcl_name);
		return;
	}
	if (gcry_cipher_get_algo_blklen(algo) == 0) {
		VERR(ctx, "Cannot determine block length for %s cipher in %s "
		     "constructor", ciphers, vcl_name);
		return;
	}

#define MODE(e, m, p, i, c) if (strcmp(modes, str(e)) == 0) mode = m;
#include "modes.h"
#undef MODE
	if (mode == GCRY_CIPHER_MODE_NONE)
		WRONG("Illegal mode enum");

#define PADDING(p) if (strcmp(paddings, str(p)) == 0) padding = p;
#include "paddings.h"
#undef PADDING
	if (padding == _MAX_PADDING)
		WRONG("Illegal padding enum");
	if (!need_padding[mode] || (mode == GCRY_CIPHER_MODE_CBC && cbc_cts))
		padding = NONE;

	if (secure)
		flags |= GCRY_CIPHER_SECURE;
	if (cbc_cts)
		flags |= GCRY_CIPHER_CBC_CTS;

	/* Create a handle to test for errors */
	if ((err = gcry_cipher_open(&hd, algo, mode, flags))
	    != GPG_ERR_NO_ERROR) {
		VERR(ctx, "Cannot open cipher in %s constructor: %s/%s",
		     vcl_name, gcry_strsource(err), gcry_strerror(err));
		return;
	}
	err = gcry_cipher_setkey(hd, key->priv, key->len);
	gcry_cipher_close(hd);
	if (err != GPG_ERR_NO_ERROR) {
		VERR(ctx, "Cannot set key in %s constructor: %s/%s",
		     vcl_name, gcry_strsource(err), gcry_strerror(err));
		return;
	}

	if (pthread_key_create(&hdk, hdk_fini) != 0) {
		if (errno == EAGAIN)
			VERR(ctx, "pthread key allocation exhausted "
			     "(PTHREAD_KEYS_MAX=%d) in %s constructor, "
			     "discard old VCL instances or restart Varnish",
			     PTHREAD_KEYS_MAX, vcl_name);
		else
			VERR(ctx, "unknown error creating pthread key in %s "
			     "constructor (errno=%d, %s)", vcl_name, errno,
			     strerror(errno));
		return;
	}

	ALLOC_OBJ(symmetric, VMOD_GCRYPT_SYMMETRIC_MAGIC);
	AN(symmetric);
	*symmetricp = symmetric;
	symmetric->hdk = hdk;
	if (secure)
		symmetric->key = gcry_malloc_secure(key->len);
	else
		symmetric->key = malloc(key->len);
	if (symmetric->key == NULL) {
		VERRNOMEM(ctx, "copying key in %s constructor", vcl_name);
		return;
	}
	memcpy(symmetric->key, key->priv, key->len);
	symmetric->vcl_name = strdup(vcl_name);
	if (symmetric->vcl_name == NULL) {
		VERRNOMEM(ctx, "copying object name in %s constructor",
			  vcl_name);
		return;
	}
	symmetric->keylen = key->len;
	symmetric->padding = padding;
	symmetric->algo = algo;
	symmetric->mode = mode;
	symmetric->flags = flags;
}

VCL_VOID
vmod_symmetric__fini(struct vmod_gcrypt_symmetric **symmetricp)
{
	struct vmod_gcrypt_symmetric *symmetric;

	if (symmetricp == NULL || *symmetricp == NULL)
		return;
	symmetric = *symmetricp;
	CHECK_OBJ(symmetric, VMOD_GCRYPT_SYMMETRIC_MAGIC);
	if (symmetric->key != NULL) {
		if (symmetric->flags & GCRY_CIPHER_SECURE)
			gcry_free(symmetric->key);
		else
			free(symmetric->key);
	}
	if (symmetric->vcl_name != NULL)
		free(symmetric->vcl_name);
	AZ(pthread_key_delete(symmetric->hdk));
	FREE_OBJ(symmetric);
}

static gcry_cipher_hd_t *
get_symmetric_hd(VRT_CTX,
		 const struct vmod_gcrypt_symmetric * const restrict symmetric,
		 const char * const restrict caller)
{
	void *p;
	gcry_cipher_hd_t *hd;
	gcry_error_t err = GPG_ERR_NO_ERROR;

	p = pthread_getspecific(symmetric->hdk);
	if (p != NULL) {
		hd = (gcry_cipher_hd_t *)p;
		if ((err = gcry_cipher_reset(*hd)) != GPG_ERR_NO_ERROR) {
			VERR(ctx, "Cannot reset cipher handle in %s.%s(): "
			     "%s/%s", symmetric->vcl_name, caller,
			     gcry_strsource(err), gcry_strerror(err));
			return NULL;
		}
		return hd;
	}

	hd = malloc(sizeof(hd));
	if (hd == NULL) {
		VERRNOMEM(ctx, "Allocating cipher handle in %s.%s()",
			  symmetric->vcl_name, caller);
		return NULL;
	}
	if ((err = gcry_cipher_open(hd, symmetric->algo, symmetric->mode,
				    symmetric->flags)) != GPG_ERR_NO_ERROR) {
		VERR(ctx, "Cannot open cipher in %s.%s(): %s/%s",
		     symmetric->vcl_name, caller, gcry_strsource(err),
		     gcry_strerror(err));
		return NULL;
	}
	if ((err = gcry_cipher_setkey(*hd, symmetric->key, symmetric->keylen))
	    != GPG_ERR_NO_ERROR) {
		VERR(ctx, "Cannot set key in %s.%s(): %s/%s",
		     symmetric->vcl_name, caller, gcry_strsource(err),
		     gcry_strerror(err));
		return NULL;
	}
	pthread_setspecific(symmetric->hdk, (void *)hd);
	return hd;
}

VCL_BLOB
vmod_symmetric_encrypt(VRT_CTX, struct vmod_gcrypt_symmetric *symmetric,
		       VCL_BLOB plainblob, VCL_BLOB iv, VCL_BLOB ctr)
{
	size_t len, blocklen;
	uintptr_t snap;
	void *plaintext;
	struct vmod_priv *ciphertext;
	gcry_error_t err = GPG_ERR_NO_ERROR;
	gcry_cipher_hd_t *hd;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(symmetric, VMOD_GCRYPT_SYMMETRIC_MAGIC);
	if (plainblob == NULL || plainblob->priv == NULL) {
		VERR(ctx, "Plaintext BLOB is NULL in %s.encrypt()",
		     symmetric->vcl_name);
		return NULL;
	}

	snap = WS_Snapshot(ctx->ws);
	if ((ciphertext = WS_Alloc(ctx->ws, sizeof(*ciphertext))) == NULL) {
		VERRNOMEM(ctx, "Allocating BLOB result in %s.encrypt()",
			  symmetric->vcl_name);
		return NULL;
	}
	blocklen = gcry_cipher_get_algo_blklen(symmetric->algo);
	AN(blocklen);
	if (symmetric->padding != NONE) {
		plaintext = (padf[symmetric->padding])(ctx->ws, plainblob->priv,
						       plainblob->len, blocklen,
						       &len);
		if (plaintext == NULL) {
			VERRNOMEM(ctx, "Allocating padded plaintext in "
				  "%s.encrypt()", symmetric->vcl_name);
			goto fail;
		}
	}
	else {
		plaintext = plainblob->priv;
		len = plainblob->len;
	}
	if ((ciphertext->priv = WS_Alloc(ctx->ws, len)) == NULL) {
		VERRNOMEM(ctx, "Allocating ciphertext result in %s.encrypt",
			  symmetric->vcl_name);
		goto fail;
	}

	hd = get_symmetric_hd(ctx, symmetric, "encrypt");
	if (hd == NULL)
		goto fail;
	if (need_iv[symmetric->mode]) {
		if (iv == NULL) {
			VERR(ctx, "Required initialization vector is NULL in "
			     "%s.encrypt()", symmetric->vcl_name);
			goto fail;
		}
		assert(iv->len >= 0);
		/* A NULL iv of length 0 is permitted. */
		if (iv->priv == NULL)
			assert(iv->len == 0);
		if ((err = gcry_cipher_setiv(*hd, iv->priv, iv->len))
		    != GPG_ERR_NO_ERROR) {
			VERR(ctx, "Cannot set initialization vector in "
			     "%s.encrypt(): %s/%s", symmetric->vcl_name,
			     gcry_strsource(err), gcry_strerror(err));
			goto fail;
		}
	}
	if (need_ctr[symmetric->mode]) {
		if (ctr == NULL || ctr->priv == NULL) {
			VERR(ctx, "Required counter vector is NULL in "
			     "%s.encrypt()", symmetric->vcl_name);
			goto fail;
		}
		assert(ctr->len >= 0);
		if ((err = gcry_cipher_setctr(*hd, ctr->priv, ctr->len))
		    != GPG_ERR_NO_ERROR) {
			VERR(ctx, "Cannot set counter vector in %s.encrypt(): "
			     "%s/%s", symmetric->vcl_name, gcry_strsource(err),
			     gcry_strerror(err));
			goto fail;
		}
	}
	if ((err = gcry_cipher_encrypt(*hd, ciphertext->priv, len, plaintext,
				       len))
	    != GPG_ERR_NO_ERROR) {
		VERR(ctx, "in %s.encrypt(): %s/%s", symmetric->vcl_name,
		     gcry_strsource(err), gcry_strerror(err));
		goto fail;
	}
	ciphertext->len = len;
	ciphertext->free = NULL;
	return ciphertext;

 fail:
	WS_Reset(ctx->ws, snap);
	return NULL;
}

VCL_BLOB
vmod_symmetric_decrypt(VRT_CTX, struct vmod_gcrypt_symmetric *symmetric,
		       VCL_BLOB ciphertext, VCL_BLOB iv, VCL_BLOB ctr)
{
	uintptr_t snap;
	struct vmod_priv *plaintext;
	gcry_error_t err = GPG_ERR_NO_ERROR;
	gcry_cipher_hd_t *hd;
	size_t blocklen;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(symmetric, VMOD_GCRYPT_SYMMETRIC_MAGIC);
	if (ciphertext == NULL || ciphertext->priv == NULL) {
		VERR(ctx, "Ciphertext BLOB is NULL in %s.decrypt()",
		     symmetric->vcl_name);
		return NULL;
	}
	assert(ciphertext->len >= 0);

	snap = WS_Snapshot(ctx->ws);
	if ((plaintext = WS_Alloc(ctx->ws, sizeof(*plaintext))) == NULL) {
		VERRNOMEM(ctx, "Allocating BLOB result in %s.decrypt()",
			  symmetric->vcl_name);
		return NULL;
	}

	plaintext->priv = WS_Front(ctx->ws);
	if (WS_Reserve(ctx->ws, 0) < (unsigned) ciphertext->len) {
		VERRNOMEM(ctx, "Allocating plaintext result in %s.decrypt()",
			  symmetric->vcl_name);
		WS_Reset(ctx->ws, snap);
		return NULL;
	}

	hd = get_symmetric_hd(ctx, symmetric, "decrypt");
	if (hd == NULL)
		goto fail;
	if (need_iv[symmetric->mode]) {
		if (iv == NULL) {
			VERR(ctx, "Required initialization vector is NULL in "
			     "%s.decrypt()", symmetric->vcl_name);
			goto fail;
		}
		assert(iv->len >= 0);
		/* A NULL iv of length 0 is permitted. */
		if (iv->priv == NULL)
			assert(iv->len == 0);
		if ((err = gcry_cipher_setiv(*hd, iv->priv, iv->len))
		    != GPG_ERR_NO_ERROR) {
			VERR(ctx, "Cannot set initialization vector in "
			     "%s.decrypt(): %s/%s", symmetric->vcl_name,
			     gcry_strsource(err), gcry_strerror(err));
			goto fail;
		}
	}
	if (need_ctr[symmetric->mode]) {
		if (ctr == NULL || ctr->priv == NULL) {
			VERR(ctx, "Required counter vector is NULL in "
			     "%s.decrypt()", symmetric->vcl_name);
			goto fail;
		}
		assert(ctr->len >= 0);
		if ((err = gcry_cipher_setctr(*hd, ctr->priv, ctr->len))
		    != GPG_ERR_NO_ERROR) {
			VERR(ctx, "Cannot set counter vector in %s.decrypt(): "
			     "%s/%s", symmetric->vcl_name, gcry_strsource(err),
			     gcry_strerror(err));
			goto fail;
		}
	}
	if ((err = gcry_cipher_decrypt(*hd, plaintext->priv, ciphertext->len,
				       ciphertext->priv, ciphertext->len))
	    != GPG_ERR_NO_ERROR) {
		VERR(ctx, "in %s.decrypt(): %s/%s", symmetric->vcl_name,
		     gcry_strsource(err), gcry_strerror(err));
		goto fail;
	}
	plaintext->len = ciphertext->len;
	blocklen = gcry_cipher_get_algo_blklen(symmetric->algo);
	AN(blocklen);
	if (symmetric->padding != NONE) {
		if (plaintext->len % blocklen != 0) {
			VERR(ctx, "in %s.decrypt(): padding is required, but "
			     "plaintext length %d is not a multiple of block "
			     "length %d", symmetric->vcl_name, plaintext->len,
			     blocklen);
			goto fail;
		}
		plaintext->len =
			(unpadlenf[symmetric->padding])(plaintext->priv,
							ciphertext->len,
							blocklen);
		if (plaintext->len < 0) {
			VERR(ctx, "in %s.decrypt(): incorrect padding",
			     symmetric->vcl_name);
			goto fail;
		}
	}
	WS_Release(ctx->ws, plaintext->len);
	plaintext->free = NULL;
	return plaintext;

 fail:
	WS_Release(ctx->ws, 0);
	WS_Reset(ctx->ws, snap);
	return NULL;
}

/* Function random */

static inline void
get_rnd(VCL_ENUM const restrict qualitys, void * const restrict p, size_t n)
{
	switch(qualitys[0]) {
	case 'N':
		AZ(strcmp(qualitys + 1, "ONCE"));
		gcry_create_nonce(p, n);
		break;
	case 'S':
		AZ(strcmp(qualitys + 1, "TRONG"));
		gcry_randomize(p, n, GCRY_STRONG_RANDOM);
		break;
	case 'V':
		AZ(strcmp(qualitys + 1, "ERY_STRONG"));
		gcry_randomize(p, n, GCRY_VERY_STRONG_RANDOM);
		break;
	default:
		WRONG("Illegal quality enum");
	}
}

VCL_BLOB
vmod_random(VRT_CTX, struct vmod_priv *rnd_task, VCL_ENUM qualitys, VCL_BYTES n)
{
	struct vmod_priv *rnd_blob;
	uintptr_t snap;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	AN(rnd_task);
	assert(n >= 0);

	if (n == 0) {
		if (rnd_task->priv == NULL) {
			ERR(ctx, "in gcrypt.random(): No random BLOB was "
			    "created previously in this task scope");
			return NULL;
		}
		assert(rnd_task->len == sizeof(*rnd_blob));
		assert(((VCL_BLOB)rnd_task->priv)->len > 0);
		return (VCL_BLOB)rnd_task->priv;
	}

	if (qualitys == NULL) {
		ERR(ctx, "in gcrypt.random(): quality ENUM is NULL");
		return NULL;
	}

	snap = WS_Snapshot(ctx->ws);
	if ((rnd_blob = WS_Alloc(ctx->ws, sizeof(*rnd_blob))) == NULL) {
		ERRNOMEM(ctx, "in gcrypt.random(), allocating space for return "
			 "BLOB");
		return NULL;
	}
	if ((rnd_blob->priv = WS_Alloc(ctx->ws, n)) == NULL) {
		WS_Reset(ctx->ws, snap);
		VERRNOMEM(ctx, "in gcrypt.random(), allocating space for %d "
			  "random bytes", n);
		return NULL;
	}
	get_rnd(qualitys, rnd_blob->priv, n);
	rnd_blob->len = n;
	rnd_blob->free = NULL;
	rnd_task->priv = rnd_blob;
	rnd_task->len = sizeof(*rnd_blob);
	rnd_task->free = NULL;
	return rnd_blob;
}

/* Function random_int */

VCL_INT
vmod_random_int(VRT_CTX, VCL_ENUM qualitys, VCL_INT bound)
{
	VCL_INT r, mask;
	unsigned nbytes = 0;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	AN(qualitys);
	assert(qualitys[0] != 'V');

	if (bound <= 0) {
		get_rnd(qualitys, &r, sizeof(VCL_INT));
		return r;
	}

	for (r = bound; r > 0; r >>= 8)
		nbytes++;
	assert(nbytes <= sizeof(VCL_INT));
	get_rnd(qualitys, &r, nbytes);
	mask = bound - 1;

	if ((bound & mask) == 0)
		/* bound is a power of 2 */
		return r & mask;

	/*
	 * Adapted from java.util.Random.nextInt -- reject values that
	 * would lead to a non-uniform distribution when the range of INT
	 * is not evenly divisible by bound.
	 */
	for (VCL_INT u = r; u - (r = u % bound) + mask < 0;
	     get_rnd(qualitys, &u, nbytes))
		;
	return r;
}

/* Function random_real */

/*
 * This makes use of knowledge that VRT_REAL is typedef double, and that
 * doubles have 53 bit mantissas on most platforms, including all of those
 * supported by Varnish.
 *
 * It probably won't work if VRT_REAL ever becomes long double, which is
 * likely to be extended precision, and won't work if the conditions
 * described in the #errors below are not satisfied. For that, we'll have
 * to make use of libm.
 */

#if FLT_RADIX != 2
#error "random_real() implementation requires FLT_RADIX == 2"
#endif

#if DBL_MANT_DIG > 63
#error "random_real() implementation requires DBL_MANT_DIG < 64"
#endif

#define DBL_BYTES ((DBL_MANT_DIG + 7) / 8)
#define DBL_INV (1./((uint64_t)1 << DBL_MANT_DIG))
#define DBL_MASK (0xffffffffffffffff >> (64 - DBL_MANT_DIG))

VCL_REAL
vmod_random_real(VRT_CTX, VCL_ENUM qualitys)
{
	VCL_REAL r;
	uint64_t u = 0;

	(void) ctx;
	AN(qualitys);
	assert(qualitys[0] != 'V');

	get_rnd(qualitys, &u, DBL_BYTES);
	u &= DBL_MASK;
	r = u * DBL_INV;
	assert(r >= 0. && r < 1.);
	return r;
}

/* Function random_bool */

/* pthread key destructor */

static void
rnd_bool_fini(void *p)
{
	struct rnd_bool *r;

	AN(p);
	CAST_OBJ(r, p, VMOD_GCRYPT_RND_BOOL_MAGIC);
	FREE_OBJ(r);
}

/*
 * Initialize the pthread key for the cached bitmaps via pthread_once,
 * only if random_bool() is called at all.
 */
static void
rnd_bool_init(void)
{
	AZ(rnd_boolk_inited);
	errno = 0;
	if (pthread_key_create(&rnd_boolk, rnd_bool_fini) != 0) {
		assert(errno == EAGAIN);
		return;
	}
	rnd_boolk_inited = 1;
}

VCL_BOOL
vmod_random_bool(VRT_CTX, VCL_ENUM qualitys)
{
	VCL_BOOL r;
	void *p;
	struct rnd_bool *rb;
	int map = 0;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	AN(qualitys);
	assert(qualitys[0] != 'V');

	if (qualitys[0] == 'S')
		map = 1;
	AZ(pthread_once(&rnd_bool_once, rnd_bool_init));
	if (! rnd_boolk_inited) {
		VERR(ctx, "pthread key allocation exhausted "
		     "(PTHREAD_KEYS_MAX=%d) in gcrypt.random_bool(), discard "
		     "old VCL instances or restart Varnish", PTHREAD_KEYS_MAX);
		return 0;
	}
	p = pthread_getspecific(rnd_boolk);
	if (p == NULL) {
		ALLOC_OBJ(rb, VMOD_GCRYPT_RND_BOOL_MAGIC);
		if (rb == NULL) {
			ERRNOMEM(ctx, "Allocating thread-specific random bits "
				 "for random_bool()");
			return 0;
		}
		AZ(pthread_setspecific(rnd_boolk, rb));
	}
	else
		CAST_OBJ(rb, p, VMOD_GCRYPT_RND_BOOL_MAGIC);

	if (rb->bitmap[map].nbits == 0) {
		get_rnd(qualitys, &rb->bitmap[map].bits,
			sizeof(rb->bitmap[map].bits));
		rb->bitmap[map].nbits = sizeof(rb->bitmap[map].bits) * 8;
	}
	r = rb->bitmap[map].bits & 0x01;
	rb->bitmap[map].bits >>= 1;
	rb->bitmap[map].nbits -= 1;
	return r;
}

/* Function wipe */

static inline void
wipe(void * const dst, size_t len, uint8_t val)
{
	volatile uint8_t *p = (volatile uint8_t *)dst;

	while (((uintptr_t)p & (sizeof(uint64_t)-1)) && len) {
		*p++ = val;
		len--;
	}
	if (len >= sizeof(uint64_t)) {
		volatile uint64_t *p64 = (volatile void *)p;
		uint64_t val64 = (uint64_t)0x0101010101010101 * val;
		do {
			*p64++ = val64;
			len -= sizeof(uint64_t);
		} while (len >= sizeof(uint64_t));
		p = (volatile void *)p64;
	}
	while (len) {
		*p++ = val;
		len--;
	}
}

VCL_VOID
vmod_wipe(VRT_CTX, VCL_BLOB b)
{
	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	if (b == NULL || b->len == 0 || b->priv == NULL) {
		ERR(ctx, "empty blob in gcrypt.wipe()");
		return;
	}
	wipe(b->priv, b->len, 0xff);
	wipe(b->priv, b->len, 0xaa);
	wipe(b->priv, b->len, 0x55);
	wipe(b->priv, b->len, 0x00);
}

/* Function fileread */

static void
filedata_free(void *p)
{
	struct filedata_head *fhead;
	struct filedata *f;

	if (p == NULL)
		return;
	fhead = p;
	f = VSLIST_FIRST(fhead);
	while (f != NULL) {
		struct filedata *next;

		CHECK_OBJ(f, VMOD_GCRYPT_FILEDATA_MAGIC);
		if (f->contents != NULL) {
			wipe(f->contents, f->len, 0xff);
			wipe(f->contents, f->len, 0xaa);
			wipe(f->contents, f->len, 0x55);
			wipe(f->contents, f->len, 0x00);
			if (secmem_enabled)
				gcry_free(f->contents);
			else
				free(f->contents);
		}
		next = VSLIST_NEXT(f, list);
		FREE_OBJ(f);
		f = next;
	}
	free(fhead);
}

VCL_BLOB
vmod_fileread(VRT_CTX, struct vmod_priv *task, VCL_STRING path)
{
	struct vmod_priv *b;
	struct filedata_head *fhead;
	struct filedata *fdata;
	struct stat st, fst;
	uintptr_t snap;
	void *contents = NULL;
	int fd = -1;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	AN(task);
	if (path == NULL) {
		ERR(ctx, "path is NULL in gcrypt.fileread()");
		return NULL;
	}
	if (!gcry_control(GCRYCTL_INITIALIZATION_FINISHED_P)) {
		ERR(ctx, "libgcrypt initialization not finished in "
		    "gcrypt.fileread()");
		return NULL;
	}

	if (task->priv == NULL) {
		fhead = calloc(1, sizeof(*fhead));
		if (fhead == NULL) {
			ERRNOMEM(ctx, "Cannot allocate file data list in "
				 "gcrypt.fileread()");
			return NULL;
		}
		VSLIST_INIT(fhead);
		task->priv = fhead;
		task->len = sizeof(*fhead);
		task->free = filedata_free;
	}
	else
		fhead = task->priv;

	snap = WS_Snapshot(ctx->ws);
	if ((b = WS_Alloc(ctx->ws, sizeof(*b))) == NULL) {
		ERRNOMEM(ctx, "Allocating return BLOB in gcrypt.fileread()");
		return NULL;
	}
	b->free = NULL;

	errno = 0;
	if (stat(path, &st) < 0) {
		VERR(ctx, "Cannot stat %s in gcrypt.fileread(): %s", path,
		     strerror(errno));
		goto fail;
	}

	if (!S_ISREG(st.st_mode)) {
		VERR(ctx, "%s is not a regular file in gcrypt.fileread()",
		     path);
		goto fail;
	}
	errno = 0;
	if ((fd = open(path, O_RDONLY)) < 0) {
		VERR(ctx, "Cannot open %s in gcrypt.fileread(): %s", path,
		     strerror(errno));
		goto fail;
	}

	if (secmem_enabled)
		contents = gcry_malloc_secure(st.st_size);
	else
		contents = malloc(st.st_size);
	if (contents == NULL) {
		VERRNOMEM(ctx, "Allocating space for contents of %s in "
			  "gcrypt.fileread()", path);
		goto fail;
	}
	for (size_t len = st.st_size, offset = 0; len > 0; ) {
		ssize_t bytes;

		errno = 0;
		bytes = read(fd, contents + offset, len);
		if (bytes < 0) {
			VERR(ctx, "Reading from %s in gcrypt.fileread(): %s",
			     path, strerror(errno));
			goto fail;
		}
		len -= bytes;
		offset += bytes;
	}

	/* TOCTOU check */
	errno = 0;
	if (fstat(fd, &fst) < 0) {
		VERR(ctx, "Cannot stat %s after read in gcrypt.fileread(): %s",
		     path, strerror(errno));
		goto fail;
	}
	if (fst.st_mtime != st.st_mtime || fst.st_mode != st.st_mode
	    || fst.st_size != st.st_size || fst.st_uid != st.st_uid
	    || fst.st_gid != st.st_gid || fst.st_dev != st.st_dev
	    || fst.st_ino != st.st_ino) {
		VERR(ctx, "%s was changed between stat and read", path);
		goto fail;
	}

	errno = 0;
	if (close(fd) < 0) {
		VERR(ctx, "Closing %s after read in gcrypt.fileread(): %s",
		     path, strerror(errno));
		fd = -1;
		goto fail;
	}
	fd = -1;

	ALLOC_OBJ(fdata, VMOD_GCRYPT_FILEDATA_MAGIC);
	if (fdata == NULL) {
		ERRNOMEM(ctx, "Saving file data in gcrypt.fileread()");
		goto fail;
	}
	fdata->contents = contents;
	fdata->len = st.st_size;
	VSLIST_INSERT_HEAD(fhead, fdata, list);
	b->priv = contents;
	b->len = st.st_size;
	return b;

 fail:
	if (contents != NULL) {
		if (secmem_enabled)
			gcry_free(contents);
		else
			free(contents);
	}
	if (fd != -1) {
		errno = 0;
		if (close(fd) < 0)
			VERR(ctx, "Closing %s in gcrypt.fileread(): %s", path,
			     strerror(errno));
	}
	WS_Reset(ctx->ws, snap);
	return NULL;
}

VCL_STRING
vmod_version(VRT_CTX __attribute__((unused)))
{
	return VERSION;
}

VCL_STRING
vmod_gcrypt_version(VRT_CTX __attribute__((unused)))
{
	AN(gcrypt_version);
	return gcrypt_version;
}
